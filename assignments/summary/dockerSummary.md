# Summary on Docker

![Docker logo](https://www.xenonstack.com/images/blog/docker-overview-architecture-application-deployment.png)

### What is Docker?
Docker is a container management service. The keywords of Docker are **develop, ship and run anywhere**. The whole idea of Docker is for developers to easily develop applications, ship them into containers which can then be deployed anywhere.

### Features of Docker
* Docker has the ability to reduce the size of development by providing a smaller footprint of the operating system via containers.
* With containers, it becomes easier for teams across different units, such as development, QA and Operations to work seamlessly across applications.
* You can deploy Docker containers anywhere, on any physical and virtual machines and even on the cloud.
* Since Docker containers are pretty lightweight, they are very easily scalable

### Docker VS Virtual Machine

![Docker logo](https://cloudblogs.microsoft.com/uploads/prod/sites/37/2019/07/Demystifying-containers_image1.png)

The main difference Docker and VM is that,In Docker, the containers running share the host OS kernel. A Virtual Machine, on the other hand, is not based on container technology. They are made up of user space plus kernel space of an operating system.</br>

### Docker image
* A Docker image is contains everything needed to run an applications as a container. This includes:
    * code
    * runtime
    * libraries
    * environment variables
    * configuration files

The image can then be deployed to any Docker environment and as a container.

**Container**: A Docker container is a running Docker image.

### Docker Architecture

Docker uses a client-server architecture. The Docker client talks to the Docker daemon, which does the heavy lifting of building, running, and distributing your Docker containers. The Docker client and daemon can run on the same system, or you can connect a Docker client to a remote Docker daemon. The Docker client and daemon communicate using a REST API, over UNIX sockets or a network interface.</br>
![ach logo](https://docs.docker.com/engine/images/architecture.svg)


### List of Docker Commands
* **docker run** – Runs a command in a new container.
* **docker start** – Starts one or more stopped containers
* **docker stop** – Stops one or more running containers
* **docker build** – Builds an image form a Docker file
* **docker pull** – Pulls an image or a repository from a registry
* **docker push** – Pushes an image or a repository to a registry
* **docker export** – Exports a container’s filesystem as a tar archive
* **docker exec** – Runs a command in a run-time container
* **docker search** – Searches the Docker Hub for images
* **docker attach** – Attaches to a running container
* **docker commit** – Creates a new image from a container’s changes