#Sumaary on Git

### what is Git?

Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.</br>
![git logo](https://git-scm.com/images/logos/1color-orange-lightbg@2x.png)

### Common Terminologies

* **Repository**: Often referred to as a Repo. This is essentially where the source code (files and folders) reside that we use Git to track.
* **Commit**: Saved changes are called commits.
* **Branch**: Simply put, a branch in Git is a lightweight movable pointer to one of these commits. The default branch name in Git is master.
As we start making commits, we are given a master branch that points to the last commit made.
* **Push**: Pushing is essentially syncing the local repository with remote (accessible over the internet) branches.
* **Merge**: This is pretty much self-explanatory. In a very basic sense, it is integrating two branches together.
* **Clone**: Again cloning is pretty much what it sounds like. It takes the entire remote (accessible over the internet) repository and/or one or more
branches and makes it available locally.
* **Fork**: Forking allows us to duplicate an existing repo under our username.

### Workflow

![workflow dia](https://user-content.gitlab-static.net/4be300d65238dd46a6a72a9eb1635a59adc6f6e1/68747470733a2f2f7170682e66732e71756f726163646e2e6e65742f6d61696e2d71696d672d3830663036353865386665653436316638383036643164386461383037623861)

### Open Source
Git is released under the GNU General Public License version 2.0, which is an open source license. The Git project chose to use GPLv2 to guarantee your freedom to share and change free software---to make sure the software is free for all its users.

### Branching and Merging
* The Git feature that really makes it stand apart from nearly every other SCM out there is its branching model.
* Git allows and encourages you to have multiple local branches that can be entirely independent of each other. The creation, merging, and deletion of those lines of development takes seconds.

### Small and Fast
Git is fast. With Git, nearly all operations are performed locally, giving it a huge speed advantage on centralized systems that constantly have to communicate with a server somewhere.</br>
Git was built to work on the Linux kernel, meaning that it has had to effectively handle large repositories from day one. Git is written in C, reducing the overhead of runtimes associated with higher-level languages. Speed and performance has been a primary design goal of the Git from the start.

### Data Assurance
The data model that Git uses ensures the cryptographic integrity of every bit of your project. Every file and commit is checksummed and retrieved by its checksum when checked back out. It's impossible to get anything out of Git other than the **exact bits you put in**.


## Git installation
### Installing on Windows
There are also a few ways to install Git on Windows. The most official build is available for download on the Git website. Just go to https://git-scm.com/download/win and the download will start automatically.

### Installing on Linux
If you’re on a Debian-based distribution, such as Ubuntu, try **apt**:</br>
<$ sudo apt install git-all></br>
For more options, there are instructions for installing on several different Unix distributions on the Git website, at https://git-scm.com/download/linux.